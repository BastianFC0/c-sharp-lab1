﻿using System;
using Istack;
namespace Stack
{
    public class MyStack : I510Stack
    {
        public char[] array {get;set;}
        public int max {get;set;}
        public MyStack(int max){
            this.array = new char[max];
            this.max = max;
        }

        public void Push(char character){
            if (array[max-1] != '\0') {
                throw new OutOfMemoryException("The Stack is already full!");
            } 
            else {
                for (int i = this.max-2; i > -1; i--){
                    this.array[i+1] = this.array[i];
                }
                this.array[0] = character;
            }
        }

        public char Pop(){
            if (this.array[0] == '\0'){
                throw new IndexOutOfRangeException("There is nothing left to pop!");
            } 
            else {
                char value = this.array[0];
                this.array[0] = '\0';
                for (int i = 0; i < this.array.Length; i++){
                    if (i == this.array.Length-1){
                        array[i] = '\0';
                        break;
                    } 
                    else {
                        array[i] = array[i+1];
                    }
                }
                return value;
            }
        }

        public char Peek(){
            return this.array[0];
        }

        public int Length(){
            int length = 0;
            for (int i = 0; i < this.array.Length; i++){
                if (this.array[i] != '\0'){
                    length += 1;
                }
            }
            return length;
        }
    }
}
