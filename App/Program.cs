﻿using System;
using Stack;
using Istack;
namespace App
{
    class Program
    {
        static bool ValidBrackets(string input){

            //Initial values to count the different types of brackets
            int bracket = 0;
            int squareBracket = 0;
            int curlyBracket = 0;

            //Counter to confirm the existance of brackets
            int existingBrackets = 0;

            MyStack stack = new MyStack(input.Length);

            //Transfer the String chars into an array
            for (int i = input.Length-1; i > -1; i--){
                stack.Push(input[i]);
                //Console.WriteLine(input[i]);
            }

            //Checks if the brackets are balanced by counting them and balancing
            //them out. If a left bracket was found without finding a right
            //bracket first, the code will not allow the counters to work as it's
            //impossible to balance
            int length = stack.Length();

            for(int i = 0; i < length; i++){
                char letter = stack.Pop();
                if (letter == '(' || letter == '[' || letter == '{'){
                    existingBrackets += 1;
                    switch (letter){
                        case '(':
                            if (bracket >= 0){
                                bracket += 1;
                            }
                            break;
                        case '[':
                            if (squareBracket >= 0){
                                squareBracket += 1;
                            }
                            break;
                        case '{':
                            if (curlyBracket >= 0){
                                curlyBracket += 1;
                            }
                            break;
                    }
                }
                if (letter == ')' || letter == ']' || letter == '}'){
                    existingBrackets += 1;
                    switch (letter){
                        case ')':
                                bracket -= 1;
                            break;
                        case ']':
                            squareBracket -= 1;
                            break;
                        case '}':
                                curlyBracket -= 1;
                            break;
                    }
                }
                //Console.WriteLine(bracket);
            }

            //Condition to check if the brackets match
            if (bracket + squareBracket + curlyBracket == 0 && existingBrackets > 0){
                return true;
            }
            else {
                return false;
            }
        }
        static void Main(string[] args)
        {
            
            Console.WriteLine("Please input a string");
            string text = Console.ReadLine();

            //Differing messages depending on the returned boolean
            if(ValidBrackets(text) == true){
                Console.WriteLine("All brackets matched");
            }
            else{
                Console.WriteLine("Not all brackets matched or no brackets present");
            }
    
        }
    }
}
